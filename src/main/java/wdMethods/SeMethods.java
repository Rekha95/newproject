package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;


import reports.HtmlReport;

public class SeMethods extends HtmlReport implements WdMethods{
	
	
	public RemoteWebDriver driver;	
	public int i = 1;
	public void startApp(String browser, String url) {
		try {
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodrivers.exe");
			driver = new FirefoxDriver();
		}
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The Browser "+browser+" Launched Successfully");
		takeSnap();
		}
		catch(WebDriverException e){
			System.out.println("Browser not launched successfully");
			
		}
	
	}

	public WebElement locateElement(String locator, String locValue) {
		try {
		switch (locator) {
		case "id": return driver.findElementById(locValue);
		case "classname": return driver.findElementByClassName(locValue);
		case "xpath": return driver.findElementByXPath(locValue);
		case "cssselector": return driver.findElementByCssSelector(locValue);
		case "tagname": return driver.findElementByTagName(locValue);
		case "partiallinktext": return driver.findElementByPartialLinkText(locValue);
		case "linktext": return driver.findElementByLinkText(locValue);
		case "name": return driver.findElementByName(locValue);
		} }
		catch(NoSuchElementException e) {
			System.out.println("Found no such" +locator +"exception");
			throw new RuntimeException();
		}
		catch(WebDriverException e) {
			System.out.println("Found webdriver exception for" +locator);
		}
		
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		driver.findElementById(locValue);
		return null;
	}

	@Override
	public void type(WebElement ele, String data) {
		ele.clear();
		try {
		ele.sendKeys(data);
		System.out.println("The Data "+ data+" Entered Successfully");
		takeSnap();
		}
		catch(NoSuchElementException e) {
			System.out.println("Element not found to send the value");
		}
		catch(Exception e) {
			System.out.println("Exception found: " + e.getMessage()) ;
		}
		
	}

	@Override
	public void click(WebElement ele) {
		String strtext = "";
		try {
		ele.click();
		System.out.println("The Element "+ele+" is clicked Successfully");
		takeSnap();}
		catch(StaleElementReferenceException e) {
			System.out.println("Element throws staleelement exception");
		}
		catch(ElementNotVisibleException e) {
			System.err.printf("Element not visible to click", "Fail");
			
		}
	}

	@Override
	public String getText(WebElement ele) {
		
		try {

			String text1 = ele.getText();
			System.out.println("Got the text" + text1 + "using gettext successfully");
			
		}
		catch (Exception e) {
			System.out.println("Unable to get the text due to the exception" + e.getMessage());
		}
		return null;
	}
	


	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dropdown1 = new Select(ele);
			dropdown1.selectByVisibleText(value);
			System.out.println("The dropdown is selected with text " + value + "Pass");
			} catch(Exception e){
				System.err.printf("Element not found for the given dropdown text", e.getMessage(), "Fail" );}
				

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
		Select dropdown2 = new Select(ele);
		dropdown2.selectByIndex(index);
		System.out.println("The dropdown is selected with index " + index + "Pass");
		} catch(Exception e){
			System.err.printf("Element not found for the given dropdown index", e.getMessage(), "Fail" );
			
		}
		

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		try {
		String title = driver.getTitle();
		if (title.contains(expectedTitle)) {
			System.out.println("Title verified");
		} else {
			System.out.println("Title not matched");
		}}catch(Exception e) {
			System.out.printf("Exception throws on "+ e.getMessage(), "Fail");
		}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		try {
			String exacttxt = ele.getText();
			if (exacttxt.equalsIgnoreCase(expectedText)) {
				System.out.printf("Text exactly matched", "Pass");
			}
			else {
				System.out.printf("Text not exactly matched", "Fail");
			}}catch(NoSuchElementException e){
				System.out.printf("Exception throws" + e.getMessage(), "Fail");}

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String pattxt = ele.getText();
		try {
			if (pattxt.contains(expectedText)) {
				System.out.printf("The expected partial text is matched", "Pass");
				
			}
			else {
				System.out.printf("The expected partial text is not matched", "Fail");
			}
		} catch(Exception e) {
			System.out.printf("Exception throws" + e.getMessage(), "Fail");
		}

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		try {
		String exactAbt = ele.getAttribute(attribute);
		if (exactAbt.equalsIgnoreCase(value)) {
			System.out.printf("Attribute exactly matched", "Pass");
		}
		else {
			System.out.printf("Attribute not exactly matched", "Fail");
		}}catch(NoSuchElementException e){
			System.out.printf("Exception throws" + e.getMessage(), "Fail");
		}
	}
	

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
	String patb = ele.getAttribute(attribute);
	try {
		if (patb.contains(value)) {
			System.out.printf("The expected partial attribute is matched", "Pass");
			
		}
		else {
			System.out.printf("The expected partial attribute is not matched", "Fail");
		}
	}
	catch(NoSuchElementException e) {
		System.out.println("Given Partial attribute does not match with DOM element ");
	}
	catch (Exception e ) {
	System.out.printf("Exception: " + e.getMessage(), "found");
	}
	}

	
	@Override
	public void verifySelected(WebElement ele) {
		try {
		if (ele.isSelected()) {
			System.out.printf("The element " + ele + "is selected", "Pass");
		}
		else {
			System.out.printf("The element " + ele + "is notselected", "Fail");
		}}
		catch(Exception e)
		{
			System.out.printf("The exception: " + e.getMessage(), "Fail");
		}
		
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
	try {	
	if (ele.isDisplayed()) {
		System.out.printf("The element " + ele + " is visible in the DOM", "Pass" );
	}
	else {
		System.out.printf("The element " + ele + " not visible in the DOM", "Fail");}
	}
	catch(WebDriverException e) {
		System.out.printf("WebDriverException: " + e.getMessage(), "Fail");
	}

	}

	@Override
	public void switchToWindow(int index) {
	try {
		Set<String> allwindowHandles = driver.getWindowHandles();
		List<String> allHandles = new ArrayList<>();
		allHandles.addAll(allwindowHandles);
		driver.switchTo().window(allHandles.get(index));
	}
	catch(NoSuchWindowException e) {
		System.out.printf("Window not found to switch", "Fail");
	}
	catch(WebDriverException e){
		System.out.println("WebDriver exception found");
	}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			System.out.printf("switched to new frame successfully", "Pass");
		}
		catch(NoSuchFrameException e){
			System.out.println("Frame not found to switch");
			
		}
		catch(WebDriverException e){
			System.out.printf("WebDriverException: " + e.getMessage(), "Fail");}

	}

	@Override
	public void acceptAlert() {
		try {
		driver.switchTo().alert().accept();
		}
		catch(NoAlertPresentException e) {
			System.out.println("Alert window not found to acceptAlert");}
		catch(UnhandledAlertException e) {
				System.out.println("Code not written to handle the Alert to accept");
	}}

	@Override
	public void dismissAlert() {
		String text = "";
		try {
		Alert alert = driver.switchTo().alert();
		alert.getText();
		alert.dismiss();
		System.out.printf("The alert " + text + "is dismissed", "Pass");
		}
		catch(NoAlertPresentException e) {
			System.out.printf("Alert window not found to dismissAlert", "Fail");
		}
		catch(UnhandledAlertException e) {
			System.out.printf("Code not written to handle the Alert to dismiss", "Fail");
			
		}

	}

	@Override
	public String getAlertText() {
		try {
		driver.switchTo().alert().getText();
		} 
		catch(Exception e) {
			System.out.println("Unable to get the text from the Alert");
		}
		return null;
	}


	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File desc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
try {
	driver.close();
	System.out.println("Browser closed successfully");
}
catch(Exception e)
{
	System.out.println("Browser not closed successfully");
	
}
	}

	@Override
	public void closeAllBrowsers() {
		try 
		{
			driver.quit();
			System.out.println("All the opened browsers are closed");
		}
		catch(Exception e){
			{
				System.out.println("All the opened browsers are not closed");
			}
			
		}
	}

}
