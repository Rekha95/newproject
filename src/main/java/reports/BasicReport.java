package reports;

import java.io.IOException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReport {
	
@Test
public void runreport() throws IOException {
	
	ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
	html.setAppendExisting(true);
	ExtentReports extent = new ExtentReports();
	extent.attachReporter(html);
	ExtentTest test = extent.createTest("Login", "Login leaftaps");
	
	test.pass("Username is entered", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		
	test.assignAuthor("Rekha");
	test.assignCategory("smoke");
	extent.flush();
}
}


