package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;

public class Window {
	
	public static void main (String [] args) {

	System.setProperty("webdriver.chrome.driver", "E:\\REKHA\\TestLeaf\\drivers\\chromedriver.exe");
	
	//Object creation
	
	ChromeDriver driver = new ChromeDriver();
	
	driver.get("http://leaftaps.com/opentaps/control/main");
	
	driver.manage().window().maximize();
	driver.findElementById("username").sendKeys("DemoSalesManager");
	driver.findElementById("password").sendKeys("crmsfa");
	driver.findElementByClassName("decorativeSubmit").click();
	driver.findElementByLinkText("CRM/SFA").click();
	
	driver.findElementByLinkText("Create Lead").click();
	driver.findElementById("createLeadForm_companyName").sendKeys("TestLeaf");
	driver.findElementById("createLeadForm_firstName").sendKeys("Jack", Keys.TAB);
	driver.findElementById("createLeadForm_lastName").sendKeys("Andrew");
	
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	driver.findElementByXPath("//img[@alt = 'Lookup']").click();
	Set<String> windowHandles = driver.getWindowHandles();
	List<String> ls = new ArrayList<>();
	ls.addAll(windowHandles);
	driver.switchTo().window(ls.get(1));
	
	String text2= driver.findElementByClassName("linktext").getText();
	driver.findElementByClassName("linktext").click();
	driver.switchTo().window(ls.get(0));
	driver.findElementByClassName("smallSubmit").click();
	
	//Verify the element value matches 
	String text = driver.findElementById("viewLead_parentPartyId_sp").getText();
	if (text.contains(text2)) {
		
		System.out.println("Verification successfull, value matched");
		
	}else {
		System.out.println("Verification failed, value not matched");

	}
	
	
	
}}