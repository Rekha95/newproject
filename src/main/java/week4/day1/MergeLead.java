package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "E:\\REKHA\\TestLeaf\\drivers\\chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("//img[@alt='Lookup']").click();

		//Switching to new window 
		
	Set<String> wh1 = driver.getWindowHandles();
	List<String> lst1 = new ArrayList<>();
	lst1.addAll(wh1);
	driver.switchTo().window(lst1.get(1));
	
	driver.findElementByName("id").sendKeys("10164");
	
	driver.findElementByXPath("//button[text()='Find Leads']").click();
	Thread.sleep(3000);
	WebDriverWait wait = new WebDriverWait(driver, 30);

	wait.until(ExpectedConditions.elementToBeClickable(driver.findElementByXPath("(//a[@class='linktext'])[1]")));

	//Click First Resulting lead
			driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
			
			//Switch back to primary window
			Set<String> wh2 = driver.getWindowHandles();
			List<String> lst2= new ArrayList<>();
			lst2.addAll(wh2);
			driver.switchTo().window(lst2.get(0));

			//Click on Icon near To Lead
			driver.findElementByXPath("//*[@id='partyIdTo']/following::a").click();
			
			//Move to new window
			Set<String> wh3 = driver.getWindowHandles();
			List<String> lst3= new ArrayList<>();
			lst3.addAll(wh3);
			driver.switchTo().window(lst3.get(1));

			//Enter Lead ID
			driver.findElementByName("id").sendKeys("10165");
			
			//Click Find Leads button
			driver.findElementByXPath("//button[text()='Find Leads']").click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(driver.findElementByXPath("(//a[@class='linktext'])[1]")));
			//Click First Resulting lead
			driver.findElementByXPath("(//a[@class='linktext'])[1]").click();

			//Switch back to primary window
			Set<String> wh4 = driver.getWindowHandles();
			List<String> lst4= new ArrayList<>();
			lst4.addAll(wh4);

			driver.switchTo().window(lst4.get(0));
			
			//Click Merge
			driver.findElementByLinkText("Merge").click();
			//Accept Alert
			
			driver.switchTo().alert().accept();
			
			
			/*
			Click Find Leads
			Enter From Lead ID
			Click Find Leads
			Verify error msg
			Close the browser (Do not log out)*/

		driver.close();
	}}
	
	