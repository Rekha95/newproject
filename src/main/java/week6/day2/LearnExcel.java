package week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcel {

	public static void main(String[] args) throws IOException {
		
		XSSFWorkbook wrkbook = new XSSFWorkbook("./Data/CreateLead.xlsx");
		
		XSSFSheet excelsheet = wrkbook.getSheet("CreateLead");
		
		/*XSSFRow row = excelsheet.getRow(1);
		
		XSSFCell cell = row.getCell(0);*/
		
		int rowcount = excelsheet.getLastRowNum();
		
		System.out.println("Row count is: " + rowcount);
		
		short columncount = excelsheet.getRow(0).getLastCellNum();
		System.out.println("Column count is: " + columncount );
		
		
		for (int i=1; i<=2; i++) {
			
			XSSFRow row = excelsheet.getRow(i);
		
		for (int j=0; j<4; j++) {
			
			XSSFCell column = row.getCell(j);
		
		String stringCellValue = column.getStringCellValue();
		System.out.println(stringCellValue);
		
		
	}
	}}}

