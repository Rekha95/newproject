package testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethod;
import wdMethods.SeMethods;

public class DeleteLeadTestNG extends ProjectSpecificMethod {
	
	
	@BeforeClass
	public void setData() {
		testcaseName = "CreateAccount";
		testDesc = "Desc-createaccTC";
		author= "Rekha";
		category= "Smoke";
	}
	
	@Test
	public void deleteLead() 
	{
	WebElement clckleadsbutton = locateElement("linktext","Leads");
	click(clckleadsbutton);
	WebElement clckfindleads = locateElement("linktext","Find Leads");
	click(clckfindleads);
	
	WebElement clickphone = locateElement("linktext", "Phone");
	click(clickphone);

	WebElement enterphonenumber = locateElement("name", "phoneNumber");
	type(enterphonenumber, "90");
	
	WebElement clckfindleadsbutton = locateElement("linktext", "Find Leads");
	click(clckfindleadsbutton);
	
	WebElement confirmresult = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
	String firstleadID = confirmresult.getText();
    System.out.println(firstleadID);
	
	WebElement firstresult = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
	click(firstresult);
	
	WebElement clckdeletebutton = locateElement("classname", "subMenuButtonDangerous");
	click(clckdeletebutton);
	
	WebElement clickfindleads1 = locateElement("linktext", "Find Leads");
	click(clickfindleads1);
	
	
	WebElement enterleadID = locateElement("name", "id");
	type(enterleadID, "10039");
}
}