package testcases;



import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethod;

public class MergeLeadTestNG extends ProjectSpecificMethod{
	
	@BeforeClass(groups="common")
	public void setData() {
		testcaseName = "MergeLead";
		testDesc = "Desc-createaccTC";
		author= "Rekha";
		category= "Smoke";
	}
	
	@Test(enabled=false,groups="regression", dependsOnMethods="testcases.EditLeadTestNG.editLead" )
	public void mergeLead() {
		
		WebElement clckleads = locateElement("linktext", "Leads");
		click(clckleads);
		WebElement clckmergeleads = locateElement("linktext", "Merge Leads");
		click(clckmergeleads);
		WebElement clckfromicon = locateElement("xpath","//img[@alt='Lookup']");
		click(clckfromicon);
		
		switchToWindow(1);
		
		/*WebElement gettextforlead = locateElement("xpath", "(//div[@class='x-form-element']/input)[1]");
		String savedlead = gettextforlead.getText();
		
		type(gettextforlead, savedlead);
		
		WebElement clckfindleadsbuttono = locateElement("xpath", "//button[text()='Find Leads']");
		click(clckfindleadsbuttono);
		
		//Click First Resulting lead
			WebElement clckfirstresult = locateElement("xpath","(//a[@class='linktext'])[1]");
			click(clckfirstresult);
			
			switchToWindow(0);
			

			//Click on Icon near To Lead
			WebElement clcktolead = locateElement("xpath","(//img[@alt='Lookup'])[2]");
			click(clcktolead);
			
			switchToWindow(1);
			

			WebElement gettexttolead = locateElement("xpath", "(//div[@class='x-form-element']/input)[1]");
			String savedtolead = gettextforlead.getText();
			
			type(gettextforlead, savedtolead);
			
			//Enter Lead ID
			WebElement enterleadid = locateElement("name","id");
			type(enterleadid,savedlead);
			
			//Click Find Leads button
			WebElement clickfindleads1 = locateElement("xpath", "//button[text()='Find Leads']");
			click(clickfindleads1);
			
			
			//Click First Resulting lead
			WebElement clickfirstresultleads1 = locateElement("(//a[@class='linktext'])[1]");
			click(clickfirstresultleads1);


			switchToWindow(0);
			
		*/
			
			//Click Merge
			/*WebElement clickmergebutton = locateElement("linkText","Merge");
			click(clickmergebutton);
			//Accept Alert
			
			acceptAlert();
			
			
			
			//Click Find Leads
			WebElement clckfindleads2=locateElement("linktext", "Find Leads");
			click(clckfindleads2);
			
			//Enter From Lead ID
			WebElement typefromID = locateElement("name", "id");
			type(typefromID,"10034");
			
			WebElement clickfindleadsbutton3 = locateElement("linktext", "Find Leads");
			click(clickfindleadsbutton3);
			*/
		

		//driver.close();
	}
	
		
		
	}


