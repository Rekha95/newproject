package testcases;


import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethod;

public class CreateLeadTC extends ProjectSpecificMethod{
	
	@BeforeClass
	public void setData() {
		testcaseName = "CreateLead";
		testDesc = "Desc-createaccTC";
		author= "Rekha";
		category= "Smoke";
	}
	@Test(groups="smoke",invocationCount=2,dataProvider="getdata", timeOut=120000)
	public void createLead(String cname, String fname, String lname) {
		//login();
		
		WebElement elecreatelead = locateElement("linktext", "Create Lead");
		click(elecreatelead);
		
		WebElement elecompanyname = locateElement("id", "createLeadForm_companyName");
		type(elecompanyname, cname);
		
		WebElement elefirstname = locateElement("id", "createLeadForm_firstName");
		type(elefirstname, fname);
		
		WebElement elelastname = locateElement("id", "createLeadForm_lastName");
		type(elelastname, lname);
		
		WebElement elesource = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(elesource,"Conference");
		
		WebElement elemarketingcam = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingIndex(elemarketingcam,4);
		
		verifyTitle("Create Lead | opentaps CRM");
		
		WebElement eleleadbutton = locateElement("classname", "smallSubmit");
		click(eleleadbutton);
			}
		
		//close();
	
	
	@DataProvider(name="getdata")  
	public String[][] dp() {
		String[][] array = new String[2][3];
		array[0][0]= "Frank";
		array[0][1]= "R";
		array[0][2]= "Amazon";
		
		array[1][0]= "Amy";
		array[1][1]= "S";
		array[1][2]= "TCS";
		return array;
	}
	
	}


	
	


