package testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethod;

public class EditLeadTestNG extends ProjectSpecificMethod {
	
	@BeforeClass(groups="common")
	public void setData() {
		testcaseName = "EditLead";
		testDesc = "Desc-createaccTC";
		author= "Rekha";
		category= "Smoke";
	}
	
	@Test (dependsOnMethods="testcases.CreateLeadTC.createLead", groups="sanity")
	public void editLead() {
		
		WebElement clckleadsbutton = locateElement("linktext","Leads");
		click(clckleadsbutton);
		WebElement clckfindleads = locateElement("linktext","Find Leads");
		click(clckfindleads);
		WebElement enterfirstname = locateElement("xpath","(//input[@name='firstName'])[3]");
		type(enterfirstname, "Nisha");
		WebElement clckfindleadsbutton = locateElement("linktext","Find Leads");
		click(clckfindleadsbutton);
		
		
		WebElement clckfirsttelement = locateElement("xpath","(//div[@class= 'x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		click(clckfirsttelement);
		
		verifyTitle(" opentaps CRM");
		
		
		WebElement clckeditbutton = locateElement("linktext","Edit");
		click(clckeditbutton);
			
		WebElement updatecompanyname = locateElement("id","updateLeadForm_companyName");
		type(updatecompanyname, "TestNew");
		
		WebElement clickupdatebutton = locateElement("classname","smallSubmit");
		click(clickupdatebutton);
		
		
		WebElement getcompanyname = locateElement("id", "viewLead_companyName_sp");
		getText(getcompanyname);
		System.out.println(getcompanyname + ": CompanyName Printed successfully");
		
		
	}
	

}
