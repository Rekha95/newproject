package week3.day2;

import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;

public class EditLead {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "E:\\REKHA\\TestLeaf\\drivers\\chromedriver.exe");
		
		//Object creation
		
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Nisha");
		driver.findElementByLinkText("Find Leads").click();
		
		
		driver.findElementByXPath("(//div[@class= 'x-grid3-cell-inner x-grid3-col-partyId']/a)[1]").click();
		String title = driver.getTitle();
		System.out.println("The title of the page is: " + title + " printed successfully");
	
		driver.findElementByLinkText("Edit").click();
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("TestNew");
		
		driver.findElementByClassName("smallSubmit").click();
		String companyname = driver.findElementById("viewLead_companyName_sp").getText();
		System.out.println(companyname + ": CompanyName Printed successfully");
		
		driver.close();
		
}}
