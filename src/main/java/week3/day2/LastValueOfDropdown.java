package week3.day2;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LastValueOfDropdown {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "E:\\\\REKHA\\\\TestLeaf\\\\drivers\\\\chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("http://leafground.com/pages/Dropdown.html");
		//driver.findElementById("dropdown1");
		
		WebElement dropdown1 = driver.findElementById("dropdown1");
		Select sc = new Select(dropdown1);
		List<WebElement> alloptions = sc.getOptions();
		int count = alloptions.size();
		sc.selectByIndex(count-1);
			
		WebElement dropdown2 = driver.findElementByName("dropdown2");
		Select sc2 = new Select(dropdown2);
		List<WebElement> alloptions2 = sc2.getOptions();
		int count2 = alloptions2.size();
		sc2.selectByIndex(count2-1);
		
		WebElement dropdown3 = driver.findElementById("dropdown3");
		Select sc3 = new Select(dropdown3);
		List<WebElement> alloptions3 = sc3.getOptions();
		int count3 = alloptions3.size();
		sc3.selectByIndex(count3-1);
		
		WebElement dropdown4 = driver.findElementByClassName("dropdown");
		Select sc4 = new Select(dropdown4);
		List<WebElement> alloptions4 = sc4.getOptions();
		int count4 = alloptions4.size();
		sc4.selectByIndex(count4-1);
		
		WebElement dropdown5 = driver.findElementByXPath("//*[@id=\"contentblock\"]/section/div[5]/select");
		Select sc5 = new Select(dropdown5);
		List<WebElement> alloptions5 = sc5.getOptions();
		int count5 = alloptions5.size();
		sc5.selectByIndex(count5-1);

		WebElement dropdown6 = driver.findElementByXPath("//*[@id=\"contentblock\"]/section/div[6]/select");
		Select sc6 = new Select(dropdown6);
		List<WebElement> alloptions6 = sc6.getOptions();
		int count6 = alloptions6.size();
		sc6.selectByIndex(count6-1);
		
		}

	}



